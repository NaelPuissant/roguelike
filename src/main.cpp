#include "Game.hpp"
#include "State.hpp"

#include <iostream>

int main() {
	Game game;

	game.gameLoop();
	return 0;
}
