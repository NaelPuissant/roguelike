#include "PlayState.hpp"
#include "StateManager.hpp"


PlayState::PlayState(StateManager& _stateManager): State(_stateManager){

	std::string level="data/lvl/Level1";
	std::string tileset="data/img/tileset.png";
	map.AddData(level,map);

	if (!map.loadtileset(tileset, sf::Vector2u(32, 32),map))
		std::cerr<<"failed to loading tileset file"<<std::endl;

}

void PlayState::handleImput(const sf::Event& event){
	player.handleImput(event);
}

void PlayState::draw(sf::RenderTarget& target) {
	target.draw(map);
	target.draw(player);
}

void PlayState::update() {

}

