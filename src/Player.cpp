#include "Player.hpp"


Player::Player() : Character("data/img/player.png", 60, 60,30,30) {

}

void Player::handleImput(const sf::Event& event){
	if(event.type == sf::Event::KeyPressed) {
		if (event.key.code == sf::Keyboard::Z){
			this->move(-1,0);
		}
		if (event.key.code == sf::Keyboard::S){
			this->move(1,0);
		}
		if (event.key.code == sf::Keyboard::D){
			this->move(0,1);
		}
		if (event.key.code == sf::Keyboard::Q){
			this->move(0,-1);
		}
	}
}

void Player::draw(sf::RenderTarget &target, sf::RenderStates states) const{
	states.transform *= getTransform();
	target.draw(*this,states);
}
