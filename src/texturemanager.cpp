#include "texturemanager.hpp"


void TextureManager::loadTexture(const std::string& name, const std::string &filename) {
	/* Load the texture */
	sf::Texture tex;
	if(!tex.loadFromFile(filename))
		std::cerr<<"Load Texture failed"<<std::endl;

	/* Add it to the list of textures */
	this->textures[name] = tex;
}

sf::Texture& TextureManager::getRef(const std::string& texture) {
	return this->textures.at(texture);
}
