#include "StateManager.hpp"

#include <cassert>

StateManager::StateManager(){

}


StateManager::~StateManager(){
	while(!m_states.empty()) {
		delete m_states.top();
		m_states.pop();
	}
}

void StateManager::pushState(State* state){
    assert(state != nullptr);
	m_states.push(state);
}

void StateManager::popState(){
	if (!m_states.empty())
		m_states.pop();
}

void StateManager::changeState(State *state){
	assert(state != nullptr);
	if (!m_states.empty())
		popState();
	pushState(state);
}

State& StateManager::peekState(){
	assert(!m_states.empty());
	return *m_states.top();
}
