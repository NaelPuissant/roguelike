#include "Character.hpp"

Character::Character(const std::string& path, int x, int y, int width, int height){

	if(!m_textureChar.loadFromFile(path))
		std::cerr<<"m_textureChar failed to load"<<std::endl;

	m_spriteChar.setTexture(m_textureChar);
	m_spriteChar.setPosition(x,y);
	m_spriteChar.setScale(width,height);

}

