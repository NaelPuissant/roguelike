#include "Collision.hpp"

bool Collision::TilesCollision(sf::Sprite& box,TileMap map) //position
{
    sf::IntRect a=box.getTextureRect();
    int i1,j1,i2,j2,i,j;
    if(a.top<0||(a.top+a.width-1)>=map.GetWidh()*32
    ||a.left<0||(a.top+a.height-1)>=map.GetHeight()*32)
        return true;
    i1=a.top/32;
    j1=a.left/32;
    i2=(a.top+a.width -1)/32;
    j2=(a.left+a.height -1)/32;
    for(i=i1;i<=i2;i++)
    {
        for(j=j1;j<=j2;j++)
        {
            if(map.GetTileNumber(i,j)==0) return true;
        }
    }
    return false;
}
bool Collision::SpriteCollision(sf::Sprite& object1,sf:: Sprite& object2)
{
    sf::IntRect a=object1.getTextureRect();
    sf::IntRect b=object2.getTextureRect();
    if((b.top>=a.top+a.width)
    ||(b.top+b.width<=a.top)
    ||(b.left>=a.left+a.height)
    ||(b.left+b.height<=a.left))
        return false;
    else return true;
}

bool Collision::TilesCollision(sf::Sprite &object, TileMap map, int tilenumber)
{
    int i1,i2,j1,j2,i,j;
    sf::IntRect a=object.getTextureRect();
    i1=a.top/32;
    j1=a.left/32;
    i2=(a.top+a.width -1)/32;
    j2=(a.left+a.height -1)/32;
    for(i=i1;i<=i2;i++)
    {
        for(j=j1;j<=j2;j++)
        {
            if(map.GetTileNumber(i,j)==tilenumber) return true;
        }
    }
    return false;
}
