#include <Game.hpp>

#include "IntroState.hpp"
#include "PlayState.hpp"

#include <SFML/Window/Event.hpp>

Game::Game() {
	m_window.create(sf::VideoMode(800,600), "Rogue");
}

Game::~Game() {
	m_window.close();
}

void Game::gameLoop(){
	m_stateManager.pushState(new PlayState(m_stateManager));
	m_stateManager.pushState(new IntroState(m_stateManager));
	while (m_window.isOpen()){
		sf::Event event;

		while (m_window.pollEvent(event)){
			if(event.type == sf::Event::Closed)
				m_window.close();
			m_stateManager.peekState().handleImput(event);
		}
		m_stateManager.peekState().update();
		m_window.clear(sf::Color::White);
		m_stateManager.peekState().draw(m_window);
		m_window.display();
	}
}

