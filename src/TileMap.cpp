#include "TileMap.hpp"


bool TileMap::AddData(std::string path,TileMap& map) {
	std::ifstream niveau;
	niveau.open(path, std::ios::in);
	if (niveau) {
		std::string ligne;
		std::vector<std::string> a_lvl;

		while (std::getline(niveau, ligne)) {
			a_lvl.push_back(ligne);
		}

		niveau.close();
		map.m_name=a_lvl[0];

		SetData(map,a_lvl);

		return true;
	} else {
		std::cerr << "Impossible d'ouvrir le niveau !" << std::endl;
		return false;
	}
}



void TileMap::SetData(TileMap& map, std::vector<std::string> a_lvl) {

	map.m_name = a_lvl[0];
	std::string t_string,t_str;
	t_str=a_lvl[1];

	while(a_lvl[1][0] !=' '){
		t_str+=a_lvl[1][0];
		a_lvl[1].erase(0,1);
	}
	a_lvl[1].erase(0,1);
	t_string=a_lvl[1];

	std::istringstream(t_str)>>map.height;
	std::istringstream(t_string) >>map.width;
	t_string.erase();
	t_string=a_lvl[1];

	a_lvl.erase(a_lvl.begin(),a_lvl.begin() +2);

	map.tiles.resize(width);

	for (unsigned int i = 0 ; i < map.width ; i++) {
		map.tiles[i].resize(height);
		for(unsigned int j = 0 ; j < map.height ; j++){
			int t = (a_lvl[i][j] - '0');
			map.tiles[i][j] = t;
		}
	}
}






 bool TileMap::loadtileset(const std::string& tileset,sf::Vector2u tileSize,TileMap& map) {

	// on charge la texture du tileset
	if (!t_tileset.loadFromFile(tileset)) {
		std::cerr<<"Le tileset n'a pas pu etre ouvrir"<<std::endl;
		return false;
	}
	// on redimensionne le tableau de vertex pour qu'il puisse contenir tout le niveau
	map.vertices.setPrimitiveType(sf::Quads);
    map.vertices.resize(map.width * map.height * 5);

	// on remplit le tableau de vertex, avec un quad par tuile
	for (unsigned int i = 0; i < map.width; ++i)
		for (unsigned int j = 0; j < height; ++j) {
			// on récupère le numéro de tuile courant
			int tileNumber = tiles[i][j];

			// on en déduit sa position dans la texture du tileset
			int tu = tileNumber % (t_tileset.getSize().x / tileSize.x);
			int tv = tileNumber / (t_tileset.getSize().x / tileSize.x);

			// on récupère un pointeur vers le quad à définir dans le tableau de vertex
			sf::Vertex* quad = &map.vertices[(i + j * map.width) * 4];

            // on définit ses quatre coins
            quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
            quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
            quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
            quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

            // on définit ses quatre coordonnées de texture
            quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
            quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
            quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
            quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);

		}
	return true;
 }

 void TileMap::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	// on applique la transformation
	states.transform *= getTransform();

	// on applique la texture du tileset
	states.texture = &t_tileset;

	// et on dessine enfin le tableau de vertex
	target.draw(vertices, states);
}

bool TileMap::IsWall(float w, float y)
{
    int i =w/32;
    int j =y/32;
    if(tiles[i][j]=0) return true;
    return false;


}

int TileMap::GetWidh()const
{
    return width;
}

int TileMap::GetHeight()const
{
    return height;
}


int TileMap::GetTileNumber(int i, int j)const
{
    return tiles[i][j];
}
