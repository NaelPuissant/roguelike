#include "gameobject.hpp"



void GameObject::load(std::string ID, std::string path, int x, int y, float width, float height)
{
    Texmgr.loadTexture(ID,path);
    sf::Vector2f scale;
    m_textureID=ID;
    m_sprite.setTexture(Texmgr.getRef(ID));
    scale.x = width / m_sprite.getTextureRect().width;
    scale.y = height / m_sprite.getTextureRect().height;
    m_sprite.setScale(scale);
    m_sprite.setPosition(x, y);
}

bool GameObject::IsColision(GameObject& object,GameObject& object2)
{
	if(object.m_sprite.getGlobalBounds().intersects(object2.m_sprite.getGlobalBounds()))
		return true;
	else
		return false;
}

