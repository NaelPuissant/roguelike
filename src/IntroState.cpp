#include "IntroState.hpp"
#include "StateManager.hpp"

#include <iostream>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Event.hpp>



IntroState::IntroState(StateManager& _stateManager) : State(_stateManager){

	//loading texture
	if (!m_textureBg.loadFromFile("data/img/bg.gif"))
		std::cerr<<"Background fail"<<std::endl;

	//set sprite
	m_bg.setTexture(m_textureBg);
	m_bg.setPosition(0,0);

	//loading font
	if (!m_fontPress.loadFromFile("data/font/press.ttf"))	//press enter font
		std::cerr<<"Font fail (press)"<<std::endl;

	if (!m_fontTitle.loadFromFile("data/font/title.ttf"))	//title font
		std::cerr<<"Font fail (title)"<<std::endl;

	//set texte
	m_press.setFont(m_fontPress);							//texte "press enter"
	m_press.setString("Press enter !");
	m_press.setPosition(300,350);
	m_press.setColor(sf::Color::Red);
	m_press.setCharacterSize(35);

	m_title.setFont(m_fontTitle);							//texte "Hoes and Dungeon"
	m_title.setString("Hoes and Dungeon");
	m_title.setPosition(100,150);
	m_title.setColor(sf::Color::Red);
	m_title.setCharacterSize(60);
}

void IntroState::draw(sf::RenderTarget& target){
	//draw member of class
	target.draw(m_bg);
	target.draw(m_title);
	target.draw(m_press);
}

void IntroState::handleImput(const sf::Event& event){
	if(event.type == sf::Event::KeyPressed) {
		if (event.key.code == sf::Keyboard::Return){
			stateManager.popState();
		}
	}
}

void IntroState::update(){

	const float blink_time = 0.5f;

	if (m_clock.getElapsedTime().asSeconds() >= blink_time){
		m_press.setColor((m_press.getColor() == sf::Color::Red) ? sf::Color::Transparent : sf::Color::Red);
		m_clock.restart();
	}
}
