#ifndef STATE_HPP
#define STATE_HPP

#include "StateManager.hpp"

namespace sf {
	class RenderTarget;
	struct Event;
}

class Game;
class State {
public:
	State(StateManager& _stateManager);
	virtual ~State();
	virtual void draw(sf::RenderTarget& target) = 0;
	virtual void handleImput(const sf::Event& event) = 0;
	virtual void update() = 0;

protected:
	StateManager& stateManager;
};

#endif // GAMESTATE_HPP
