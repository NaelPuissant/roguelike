#ifndef STATEMANAGER_HPP
#define STATEMANAGER_HPP



#include <stack>

class State;
class StateManager final{
public:
	StateManager();
	~StateManager();

	void pushState(State* state);
	void popState();
	void changeState(State* state);
	State& peekState();

private:
		std::stack<State*> m_states;
};

#endif // STATEMANAGER_HPP
