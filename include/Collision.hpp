#ifndef COLLISION_HPP
#define COLLISION_HPP
#include<SFML/Graphics/Sprite.hpp>
#include<SFML/Graphics/Rect.hpp>
#include<TileMap.hpp>
class Collision
{
public:
    Collision();
    bool TilesCollision(sf::Sprite& object, TileMap map);
    bool SpriteCollision(sf::Sprite& object1,sf::Sprite& object2);
    //For know if our sprite touch a specific tile
    bool TilesCollision(sf::Sprite& object,TileMap map,int tilenumber);
};
#endif // COLLISION_HPP
