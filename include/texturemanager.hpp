#ifndef TEXTUREMANAGER_HPP
#define TEXTUREMANAGER_HPP

#include<SFML/Graphics/Texture.hpp>


#include <string>
#include <map>
#include<iostream>


class TextureManager {
public:

	/* Load a texture from file */
	void loadTexture(const std::string& name, const std::string &filename);
	/* Translate an id into a reference */
	sf::Texture& getRef(const std::string& texture);


private:

	/* Array of texture */
	std::map<std::string, sf::Texture> textures;

};

#endif /* TEXTURE_MANAGER_HPP */

