#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP

#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/VertexArray.hpp>


#include "texturemanager.hpp"

class GameObject : public sf::Drawable, public sf::Transformable {
public:
	void update();
	void load(std::string ID, std::string path, int x, int y, float width, float height);
	bool IsColision(GameObject& object,GameObject& object2);
private:

	std::string m_textureID;
	std::string m_name;
	sf::Sprite m_sprite;
	sf::Vector2f m_moveVect;
	TextureManager Texmgr;
};

#endif // GAMEOBJECT_HPP
