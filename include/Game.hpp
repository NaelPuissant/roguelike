#ifndef GAME_HPP
#define GAME_HPP

#include "StateManager.hpp"

#include <SFML/Graphics/RenderWindow.hpp>

class State;
class Game {
	public:
		Game();
		~Game();

		void gameLoop();

	private:
		sf::RenderWindow m_window;
		StateManager m_stateManager;

};

#endif // GAME_HPP
