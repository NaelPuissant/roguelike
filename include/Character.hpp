#ifndef CHARACTER_HPP
#define CHARACTER_HPP
#include "gameobject.hpp"
#include <SFML/System/Vector2.hpp>

class Character : public GameObject
{
public:
	Character(const std::string& path, int x,int y,int width,int height);

private:
	sf::Texture m_textureChar;
	sf::Sprite m_spriteChar;

};

#endif // CHARACTER_HPP
