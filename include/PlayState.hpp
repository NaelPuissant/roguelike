#ifndef PLAYSTATE_HPP
#define PLAYSTATE_HPP

#include "State.hpp"
#include "TileMap.hpp"
#include "Player.hpp"

class PlayState : public State {

public:

	PlayState(StateManager& _stateManager);
	void draw(sf::RenderTarget& target);
	void handleImput(const sf::Event& event);
	void update();

private:
	TileMap map;
	Player player;

};

#endif // PLAYSTATE_HPP
