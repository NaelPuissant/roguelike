#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Window/Event.hpp>

#include "Character.hpp"

class Player : public Character {
public:
	Player();

	void handleImput(const sf::Event& event);
	void draw(sf::RenderTarget &target, sf::RenderStates states) const;
};

#endif // PLAYER_HPP
