#ifndef TILEMAP_HPP
#define TILEMAP_HPP

#include<vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include<SFML/Graphics/RenderTarget.hpp>

#include"gameobject.hpp"

class TileMap : public GameObject {
public:
    int GetHeight() const;
    int GetWidh() const;
    int GetTileNumber(int i, int j) const;
	bool loadtileset(const std::string& tileset,sf::Vector2u tileSize,TileMap& map);
	bool AddData(std::string path,TileMap& map);
    bool IsWall(float w, float y);
	void SetData(TileMap& map,std::vector<std::string> a_lvl);
	virtual void draw(sf::RenderTarget& target,sf::RenderStates states)const;
private:
	std::string m_name;
	unsigned int height,width;
	sf::VertexArray vertices;
	sf::Texture t_tileset;
	typedef std::vector<std::vector<int>> TileArray;
	TileArray tiles;
};
#endif // TILEMAP_HPP
