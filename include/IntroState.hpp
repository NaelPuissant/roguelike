#ifndef INTROSTATE_HPP
#define INTROSTATE_HPP

#include "State.hpp"

#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>

class IntroState : public State {

public:

	IntroState(StateManager& _stateManager);
	void draw(sf::RenderTarget& target);
	void handleImput(const sf::Event& event);
	void update();

private:
	sf::Texture m_textureBg;
	sf::Sprite m_bg;
	sf::Font m_fontPress;
	sf::Font m_fontTitle;
	sf::Text m_title;
	sf::Text m_press;
	sf::Clock m_clock;

};

#endif // INTROSTATE_HPP
